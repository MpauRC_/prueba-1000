const CryptoJS =  require('crypto-js')
const { cargarUsuarios, guardarUsuarios } = require('./archivo.js');

// Semilla de encriptación necesaria para la libreria Crypto-js
const seed = 'semilla-de-encriptacion';

/**
 * Verfica si el usuario ya esta registrado, si ya esta guardado en usuarios.json de vuelve un mensaje y se vuelve a intentar con otro usuario
 * Despues crea la cuenta definitva encriptando la contraseña y creando sus estadisticas mandado el usuario a guardar usuario en archivo.js
 */
const crearUsuario = (user) => {
    // Cargamos los usuarios
    const { usuarios } = cargarUsuarios();
    //Verificación usuarios registrados
    if (usuarios.length > 0) {
        //Ciclo usuario u se encuentra en la carga de usuarios
        for (const u of usuarios) {
            //Si el usuario u es igual a algun usuario de los cargados
            if (u.username === user.username) {
                //Retorna objeto de validación
                return {
                    ok: false,
                    message: 'El usuario ya se encuentra registrado.'
                };
            }
        }
    }

    //Encriptación 
    user.password = CryptoJS.AES.encrypt(user.password, seed).toString(CryptoJS.format.OpenSSL);
    user.estadisticas = {
        jugados: 0,
        ganados: 0,
        intentos: []
    };

    for (let i = 1; i < 7; i++) {
        user.estadisticas.intentos.push({
            intento: i,
            completados: 0
        });
    }

    usuarios.push(user);
    guardarUsuarios({
        usuarios: usuarios
    });

    return {
        ok: true,
        message: 'Usuario registrado satisfactoriamente.'
    }
}

/**
 * La función nos ayuda a ver si el usuario y/o contraseña estan en usuarios.json
 */
const verificarUsuario = (user) => {
    //Cargamos el usuario
    const { usuarios } = cargarUsuarios();

    if (usuarios.length > 0) {
        let index = 0;
        for (const u of usuarios) {
            if (u.username === user.username) {
                const decrypt = CryptoJS.AES.decrypt(u.password, seed).toString(CryptoJS.enc.Utf8);
                if (user.password === decrypt) {
                    return {
                        ok: true,
                        message: 'Sesión iniciada correctamente.',
                        index: index
                    }
                } else {
                    return {
                        ok: false,
                        message: 'Contraseña incorrecta. Intente de nuevo.'
                    }
                }
            }

            index++;
        }
    }

    return {
        ok: false,
        message: 'El usuario ingresado no se encuentra registrado.'
    }
}

module.exports = {
    crearUsuario,
    verificarUsuario
}

