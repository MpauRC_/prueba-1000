//Importar
const chalk = require('chalk');
const { cargarUsuarios, guardarUsuarios, cargarPalabras } = require('./archivo.js');
const { inputPalabra } = require('./input.js');

// Almacena el numero de letras con las que se va a jugar el juego
// En este caso 5
const LETRAS = 5;
// Numero máximo de intentos permitidos
// En este caso 6
const INTENTOS = 6;
// Arreglo que almacena las palabras cargadas desde el archivo.
// En este caso un diccionario de 20 palabras
const diccionario = cargarPalabras();

//Función encargada de manjear los caracteres 
const contarCaracteres = (caracter, palabra, posicion) => {
    let veces = 0;
    for (let i = 0; i < posicion; i++) {
        if (palabra[i] === caracter) {
            veces++;
        }
    }

    return veces;
}

// Función encargada de iniciar el juego.
const iniciarJuego = async (indexUser) => {
    //Aleatorio del diccionario para mandar la palabra generada
    const aleatorio = Math.floor(Math.random()*diccionario.length);
    const palabraGenerada = diccionario[aleatorio].name;
    const intentos = [];
    let numIntentos = 0;
    let aciertos = 0;

    // Ciclo que valida que el numero de letras de la palabra ingresada
    // sean 5 y estén correctas
    while (numIntentos < INTENTOS && aciertos !== LETRAS) {
        // Almacena la palabra ingresada por el jugador y se convierte a mayúsculas.
        let { palabraIngresada } = await inputPalabra();
        palabraIngresada = palabraIngresada.toUpperCase();

        // Se valida que la palabra contenga 5 letras
        if (palabraIngresada.length < LETRAS) {
            console.log('No hay suficientes letras para una palabra.');
            continue;
        }

        const found = diccionario.find(p => palabraIngresada === p.name);

        // Se valida que la palabra esté en el diccionario.
        if (!found) {
            console.log('La palabra no está registrada en el diccionario.');
            continue;
        }
        // Arreglo que almacena la informacion correspondiente de las 5 letras de la palabra ingresada
        // Por cada intento.
        const intento = [];
        // Se reinicia el número de letras acertadas.
        aciertos = 0;
        // Ciclo que recorre cada una de las letras de la palabra ingresada.
        for (let i = 0; i < LETRAS; i++) {
            // Cracter en la posicion
            const letraIngresada = palabraIngresada.charAt(i);
            //console.log(contarCaracteres(letraIngresada, palabraIngresada, i + 1), contarCaracteres(letraIngresada, palabraGenerada, LETRAS));
            
            
            // Se valida que la letra en la posición i está incluida en la palabra generada.
            //if (palabraGenerada.includes(letraIngresada)) {
            if (palabraGenerada.includes(letraIngresada) && contarCaracteres(letraIngresada, palabraIngresada, i + 1) <= contarCaracteres(letraIngresada, palabraGenerada, LETRAS)) {
                // Cracter en la posicion
                const letraGenerada = palabraGenerada.charAt(i);
                // Se valida que la letra i de la palabra ingresada esté en la palabra 
                // Y en la posición correcta.
                if (letraGenerada === letraIngresada) {
                    //PARÁMETROS
                    // 1) esta = Letra en la palabra original
                    // 2) correcta = Letra en la posición correcta
                    // 3) letra = Letra original
                    // 4) coloreada = Letra coloreada
                    intento.push({
                        esta: true,
                        correcta: true,
                        letra: letraIngresada,
                        coloreada: chalk.green(letraIngresada)
                    });
                    aciertos++;
                } else {
                    intento.push({
                        esta: true,
                        correcta: false,
                        letra: letraIngresada,
                        coloreada: chalk.magenta(letraIngresada)
                    });
                    aciertos = 0;
                }
            } else {
                intento.push({
                    esta: false,
                    correcta: false,
                    letra: letraIngresada,
                    coloreada: chalk.red(letraIngresada)
                });
                aciertos = 0;
            }
        }

        // Se le incluye la información del intento
        intentos.push(intento);
        numIntentos++;

        // Se imprime el tablero y el teclado correspondiente según el intento
        mostrarTablero(intentos);
        mostrarTeclado(intentos);
    }

    //validación de Juego ganado
    if (aciertos === LETRAS) {
        console.log(`Felicidades. Ha completado la palabra en ${ numIntentos } intento(s) 😺.`);
    //validación de Juego pérdido
    } else {
        console.log(`Has fallado. No te preocupes, en la proxima será 😉.`);
        //Impresión de la palabra generada aleatoriamente
        console.log(`La palabra del día es: ${ palabraGenerada }`);
    }

    //Se guardan las estadísticas dependiendo los intentos y el usuario 
    guardarEstadisticas(numIntentos, indexUser);
}

// Función encargada de mostrar el tablero de intentos en pantalla.
const mostrarTablero = (tablero) => {
    let i = 0;
    // Muestra las cajas cada una con su respectiva letra
    for (const intento of tablero) {
        let fila = '';
        for (const letra of intento) {
            fila += `[${ letra.coloreada }]`;
        }
        console.log('          ', fila);
        i++;
    }

    // Imprime en pantalla las cajas vacias
    while (i < INTENTOS) {
        let fila = '[ ][ ][ ][ ][ ]';
        console.log('          ', fila);
        i++;
    }
}

// Función encargada de mostrar y colorear el teclado según las letras indicadas.
const mostrarTeclado = (tablero) => {
    // Array que contiene los elementos del teclado.
    const teclado = [
        [{ letra: ' ', valor: ' ' }, { letra: 'Q', valor: 'Q' }, { letra: 'W', valor: 'W' }, { letra: 'E', valor: 'E' }, { letra: 'R', valor: 'R' }, { letra: 'T', valor: 'T' }, { letra: ' ', valor: ' ' }, { letra: 'Y', valor: 'Y' }, { letra: 'U', valor: 'U' }, { letra: 'I', valor: 'I' }, { letra: 'O', valor: 'O' }, { letra: 'P', valor: 'P' }],
        [{ letra: ' ', valor: ' ' }, { letra: 'A', valor: 'A' }, { letra: 'S', valor: 'S' }, { letra: 'D', valor: 'D' }, { letra: 'F', valor: 'F' }, { letra: 'G', valor: 'G' }, { letra: ' ', valor: ' ' }, { letra: 'H', valor: 'H' }, { letra: 'J', valor: 'J' }, { letra: 'K', valor: 'K' }, { letra: 'L', valor: 'L' }, { letra: 'Ñ', valor: 'Ñ' }],
        [{ letra: 'ENVIAR', valor: 'ENVIAR' }, { letra: 'Z', valor: 'Z' }, { letra: 'X', valor: 'X' }, { letra: 'C', valor: 'C' }, { letra: 'V', valor: 'V' }, { letra: 'B', valor: 'B' }, { letra: 'N', valor: 'N' }, { letra: 'M', valor: 'M' }, { letra: 'BORRAR', valor: 'BORRAR' }],
    ];

    // Se encargan de colorear las letras del teclado por cada intento que haya hecho el jugador.
    for (const intento of tablero) {
        for (const letra of intento) {
            for (const fila of teclado) {
                fila.forEach((l, i) => {
                    if (l.letra === letra.letra) {
                        fila[i].valor = letra.coloreada;
                    }
                });
            }
        }
    }

    // Muestra en pantalla el teclado.
    for (const fila of teclado) {
        let mostrar = '';
        for (const letra of fila) {
            mostrar += ` ${letra.valor} `
        }
        console.log(mostrar);
    }

    console.log();
}

//Función encargada de consultar las estadíticas de cada jugador

const consultarEstadisticas = (indexUser) => {
    //Cargamos los usuarios
    const { usuarios } = cargarUsuarios();
    //Cargamos el usuario correspondiente
    const usuario = usuarios[indexUser];
    // Cargamos las estadisticas del usuario correspondiente
    const estadisticas = usuario.estadisticas;
    // Cragamos las estadisticas en dependencia del usuario
    const intentos = estadisticas.intentos;
    const distribucion = [];

    //Validación de si no hay estadísticas
    if (estadisticas.jugados === 0) {
        console.log('No hay estadisticas para mostrar.');
        return;
    }

    // Se realiza el proceso de calcular las estadisticas correspondientes.
    for (const intento of intentos) {
        //Validación de 6 intentos
        if (intento.intento === 6) {
            // Validación del intento 
            if (intento.completados === 0) {

                //Distribución pérdidas
                distribucion.push({
                    label: 'X: ',
                    content: '(0%)'
                });
                //Cajitas de estadísticas que se verifican a nivel de el intento completado sobre los jugados
            } else {
                
                const numCajas = Math.round(intento.completados / estadisticas.jugados * 10);
                let content = '';

                for (let i = 0; i < numCajas; i++) {
                    content += `[${ chalk.bgWhite('XX') }]`;
                }

                content += `(${ (intento.completados / estadisticas.jugados * 100).toFixed(2) }%)`
                distribucion.push({
                    label: 'X: ',
                    content: content
                });
            }
        } else {
            if (intento.completados === 0) {
                distribucion.push({
                    label: `${ intento.intento }: `,
                    content: '(0%)'
                });
            } else {
                const numCajas = Math.round(intento.completados / estadisticas.jugados * 10);
                let content = '';

                for (let i = 0; i < numCajas; i++) {
                    content += `[${ chalk.bgWhite('XX') }]`;
                }

                content += `(${ (intento.completados / estadisticas.jugados * 100).toFixed(2) }%)`
                distribucion.push({
                    label: `${ intento.intento }: `,
                    content: content
                });
            }
        }
    }

    console.log(`${ estadisticas.jugados } Jugadas                        ${ ((estadisticas.ganados / estadisticas.jugados) * 100).toFixed(2) }% Victorias`);

    for (const intento of distribucion) {
        console.log(intento.label, intento.content);
    }
}

//Función que guarda las estadísticas del usuario
const guardarEstadisticas = (numIntentos, indexUser) => {
    //Cargamos los usuarios
    const { usuarios } = cargarUsuarios();
    //Cargamos el usuario correspondiente
    const usuario = usuarios[indexUser];

    //Validación 
    if (numIntentos < INTENTOS) {
        usuario.estadisticas.ganados++;
    }
    //Se validan la cantidad de pérdidas en x intento
    usuario.estadisticas.jugados++;
    usuario.estadisticas.intentos[numIntentos - 1].completados++;
    usuarios[indexUser] = usuario;

    guardarUsuarios({
        usuarios: usuarios
    });
}

module.exports = {
    iniciarJuego,
    consultarEstadisticas
}