//Importamos funciones
const { menuPrincipal, menuJugar, inputLogin, inputRegistrar } = require('./input.js');
const { iniciarJuego, consultarEstadisticas } = require('./juego.js');
const { crearUsuario, verificarUsuario } = require('./usuario.js');

/**
 * Esta en la función encargada del manejo de inicio de sesion del jugador
 */
async function iniciarSesion() {
    const usuario = await inputLogin();
    const verificado = verificarUsuario(usuario);
    console.log(verificado.message);

    if (verificado.ok) {
        await jugar(verificado.index);
    }
}

/**
 * La función está encargada de registrar un nuevo usuario.
 */
async function registrar() {
    const usuario = await inputRegistrar();
    const registrado = crearUsuario(usuario);
    console.log(registrado.message);
}

/**
 * Está función está encargada del manejo del menu del juego.
 */
async function jugar(indexUser) {
    let opcion;
    do {
        opcion = await menuJugar();
        switch(opcion.opcion) {
            case 1:
                await iniciarJuego(indexUser);
                break;
            case 2:
                consultarEstadisticas(indexUser);
                break;
            default:
                break;
        }
    } while(opcion.opcion !== 3);
}

/**
 * Aquí se encuentra el main, en donde se llaman las funciones para el funcionamiento del juego
 */
async function main() {
    let opcion;
    do {
        opcion = await menuPrincipal();
        switch(opcion.opcion) {
            case 1:
                await iniciarSesion();
                break;
            case 2:
                await registrar();
                break;
            default:
                break;
        }
    } while(opcion.opcion != 3);
}

main();