const inquirer = require('inquirer');
/**
 * Menú principal del juego en donde el usuario puede iniciar sesión, registrarse o salir del juego.
 * @returns opciones
 */
const menuPrincipal = async () => {
    const opciones = {
        name: 'opcion',
        type: 'rawlist',
        message: 'Seleccione su opción: ',
        choices: [
            { value: 1, name: 'Iniciar Sesión' }, 
            { value: 2, name: 'Registrar' },
            { value: 3, name: 'Salir' }
        ]
    }
    return inquirer.prompt(opciones);
}

/**
 * Esta función es el menú del juego de un usuario ya registrado
 * @returns opciones
 */
const menuJugar = async () => {
    const opciones = {
        name: 'opcion',
        type: 'rawlist',
        message: 'Seleccione su opción: ',
        choices: [
            { value: 1, name: 'Crear nuevo juego' }, 
            { value: 2, name: 'Consultar estadisticas' },
            { value: 3, name: 'Cerrar sesión' }
        ]
    }
    return inquirer.prompt(opciones);
}

/**
 * En la función Login podemos como se reciben el usuario y la contraseña de un usuario regsitrado previamente
 * @returns user
 * @returns password
 */
const inputLogin = async () => {
    const user = {
        name: 'username',
        type: 'input',
        message: 'Digite su nombre de usuario: '
    }

    const password = {
        name: 'password',
        type: 'password',
        message: 'Digite su contraseña: '
    }

    return inquirer.prompt([user, password]);
}
/**
 * La función nos sirve para que un usuario anómino se pueda registrar en el juego
 * @returns nombres
 * @returns user
 * @returns password
 */
const inputRegistrar = async () => {
    const nombres = {
        name: 'nombres',
        type: 'input',
        message: 'Digite su nombre: '
    }

    const user = {
        name: 'username',
        type: 'input',
        message: 'Digite su nombre de usuario: '
    }

    const password = {
        name: 'password',
        type: 'password',
        message: 'Digite su contraseña: '
    }

    return inquirer.prompt([nombres, user, password]);
}

//Función encargada de tomar el ingreso de las palabras 
//para jugar
const inputPalabra = async () => {
    const palabra = {
        name: 'palabraIngresada',
        type: 'input',
        message: 'Digite una palabra de 5 letras: '
    }
    return inquirer.prompt(palabra);
}

//Exportamos las funciones
module.exports = {
    menuPrincipal,
    menuJugar,
    inputLogin,
    inputRegistrar,
    inputPalabra
}