const fs = require('fs');
const path = require('path');

/**
 * En estas constantes se guardaran los rutas a los .json creados para guardar la información
 */
const rutaArchivoUsuarios = path.join(__dirname, 'data/usuarios.json');
const rutaArchivoPalabras = path.join(__dirname, 'data/palabras.json');

/**
 * Función encargada de encontrar en palabras.json
 * @returns palabras
 */
const cargarPalabras = () => {
    if (fs.existsSync(rutaArchivoPalabras)) {
        const file = fs.readFileSync(rutaArchivoPalabras);
        const { palabras } = JSON.parse(file);
        return palabras;
    }
}
/**
* Función encargada de cargar los usuarios y encontrarlos en Usuarios.json
* @returns usuarios: []
*/
const cargarUsuarios = () => {
    if (fs.existsSync(rutaArchivoUsuarios)) {
        try {
            const file = fs.readFileSync(rutaArchivoUsuarios);
            return JSON.parse(file);
        } catch (error) {
            return {
                usuarios: []
            }
        }
    }
}
/**
 * Guarda los usuarios definitivamente en usuarios usuarios.jsonn
 * @param {*} data 
 */
const guardarUsuarios = (data) => {
    fs.writeFileSync(rutaArchivoUsuarios, JSON.stringify(data));
}

//Exportamos las funciones
module.exports = {
    cargarPalabras,
    cargarUsuarios,
    guardarUsuarios
}